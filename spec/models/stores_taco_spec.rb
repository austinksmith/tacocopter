require 'rails_helper'

describe StoresTaco, :type => :model do
  describe 'Test Relations' do
    it "Should have records" do
      expect(StoresTaco.all).to_not be_nil
    end

    it "Should have store" do
      expect(StoresTaco.first.store).to_not be_nil
    end

    it "Should have tacos" do
      expect(StoresTaco.first.taco).to_not be_nil
    end
  end

end
