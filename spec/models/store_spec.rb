require 'rails_helper'

describe Store, :type => :model do
  describe 'Test Relations' do
    it "Should have records" do
      expect(Store.all).to_not be_nil
    end

    it "Stores should have city" do
      expect(Store.first.city).to_not be_nil
    end

    it "Stores should have tacos" do
      expect(Store.first.taco).to_not be_nil
    end

    it "Stores should have salsa" do
      expect(Store.first.taco).to_not be_nil
    end
  end

end
