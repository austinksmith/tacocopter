require 'rails_helper'

describe Salsa, :type => :model do
  describe 'Test Relations' do
    it "Should have records" do
      expect(Salsa.all).to_not be_nil
    end

    it "Salsa should have store" do
      expect(Salsa.first.store).to_not be_nil
    end
  end

end
