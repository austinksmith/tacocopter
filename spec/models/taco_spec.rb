require 'rails_helper'

describe Taco, :type => :model do
  describe 'Test Relations' do
    it "Should have records" do
      expect(Taco.all).to_not be_nil
    end

    it "Taco should have store" do
      expect(Taco.first.store).to_not be_nil
    end
  end

end
