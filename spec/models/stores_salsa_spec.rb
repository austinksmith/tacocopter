require 'rails_helper'

describe StoresSalsa, :type => :model do
  describe 'Test Relations' do
    it "Should have records" do
      expect(StoresSalsa.all).to_not be_nil
    end

    it "Should have store" do
      expect(StoresSalsa.first.store).to_not be_nil
    end

    it "Should have salsa" do
      expect(StoresSalsa.first.salsa).to_not be_nil
    end
  end

end
