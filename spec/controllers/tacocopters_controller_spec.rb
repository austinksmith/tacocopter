require 'rails_helper'

describe TacocoptersController, :type => :controller do

  describe 'Taco Controller Actions' do
    it "should render index" do
      get :index
      expect(response).to be_success
    end

    it "should create @tacos" do
      get :index
      assigns(:tacos).should_not be_nil
    end

    it "should create @salsas" do
      get :index
      assigns(:salsas).should_not be_nil
    end

    it "should search for single taco and salsa" do
      post :search, "taco" => ["Al Pastor"], "salsa" => ["Pico de Gallo"]
      expect(response).to be_success
    end

    it "should search for multiple tacos and salsas" do
      post :search, "taco" => ["Al Pastor", "Fillet Mignon"], "salsa" => ["Pico de Gallo", "Salsa Roja"]
      expect(response).to be_success
    end
  end
end
