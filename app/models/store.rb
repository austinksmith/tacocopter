class Store < ActiveRecord::Base
  #Has many taco stores
  has_many :stores_taco
  #Has many salsa stores
  has_many :stores_salsa
  #Has many tacos
  has_many :taco, through: :stores_taco
  #Has many salsa
  has_many :salsa, through: :stores_salsa
  #Store belongs to city (not multiple cities)
  belongs_to :city
end
