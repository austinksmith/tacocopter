class City < ActiveRecord::Base
  #Each city has many stores
  has_many :store
end
