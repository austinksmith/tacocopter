class Salsa < ActiveRecord::Base
  has_many :stores_salsa
  has_many :store, through: :stores_salsa
end
