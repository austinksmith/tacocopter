class Taco < ActiveRecord::Base
  has_many :stores_taco
  has_many :store, through: :stores_taco
end
