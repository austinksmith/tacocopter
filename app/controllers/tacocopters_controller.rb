class TacocoptersController < ApplicationController
  before_action :set_tacocopter, only: [:show, :edit, :update, :destroy]

  # GET /tacocopters
  # GET /tacocopters.json
  def index
    # binding.pry
    @tacos = Taco.all
    @salsas = Salsa.all
    render :index
  end

  # GET /tacocopters/1
  # GET /tacocopters/1.json
  def show
    respond_to do |format|
      format.html {
        render :show
      }
      format.json { 
        render json: @results
      }
    end
  end

  # GET /tacocopters/new
  def new
    @tacocopter = Tacocopter.new
  end

  # GET /tacocopters/1/edit
  def edit
  end

  def search
    if !params[:taco].present? || !params[:salsa].present?
      redirect_to :root
      return
    end
    results = Store.includes(:taco, :salsa).where(:tacos => {:name => params[:taco]}, :salsas => {:name => params[:salsa]})
    if results.size > 0 
      results = results.order('zagat_rating desc')
    end
    @results = results
    render :show
  end

  # POST /tacocopters
  # POST /tacocopters.json
  def create
    @tacocopter = Tacocopter.new(tacocopter_params)

    respond_to do |format|
      if @tacocopter.save
        format.html { redirect_to @tacocopter, notice: 'Tacocopter was successfully created.' }
        format.json { render :show, status: :created, location: @tacocopter }
      else
        format.html { render :new }
        format.json { render json: @tacocopter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tacocopters/1
  # PATCH/PUT /tacocopters/1.json
  def update
    respond_to do |format|
      if @tacocopter.update(tacocopter_params)
        format.html { redirect_to @tacocopter, notice: 'Tacocopter was successfully updated.' }
        format.json { render :show, status: :ok, location: @tacocopter }
      else
        format.html { render :edit }
        format.json { render json: @tacocopter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tacocopters/1
  # DELETE /tacocopters/1.json
  def destroy
    @tacocopter.destroy
    respond_to do |format|
      format.html { redirect_to tacocopters_url, notice: 'Tacocopter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    #Database contains duplicate stores, return the ones with highest rating.
    def remove_duplicates(stores)
     unique = []
     stores.each do |store|

     end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_tacocopter
      @tacocopter = Tacocopter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tacocopter_params
      params[:tacocopter]
    end
end
