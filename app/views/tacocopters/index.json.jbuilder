json.array!(@tacocopters) do |tacocopter|
  json.extract! tacocopter, :id
  json.url tacocopter_url(tacocopter, format: :json)
end
